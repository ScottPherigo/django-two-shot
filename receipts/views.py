from django.shortcuts import render
from receipts.models import Receipt

# Create your views here.
def show_receipts(request):
    show_receipts = Receipt.objects.all()
    context = {
        "receipt": show_receipts,
    }
    return render(request, "receipts/home.html", context)